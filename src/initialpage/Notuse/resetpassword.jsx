/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {Applogo} from '../../Entryfile/imagepath.jsx'
import PasswordMask from 'react-password-mask';

class Loginpage extends Component {
    constructor(props) {
    super(props);

    this.state = {
        password : ''
    };
    this.handleChange = this.handleChange.bind(this)
  }
   state = {
      email: 'demo@example.com',
      password: 'test#123'
   }

  // Input box Type method
  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
  }
      
   loginclick(){
    // this.props.history.push("/app/main/dashboard")
    localStorage.setItem("firstload","true")
   }


   render() {
     
      return (
         
         
         <div className="main-wrapper">
           <Helmet>
               <title>Login - PARTICIPATION HOUSE SUPPORT SERVICES</title>
               <meta name="description" content="Login page"/>					
         </Helmet>
        <div className="account-content">
          
          <div className="container">
            {/* Account Logo */}
            <div className="account-logo">
              <a href="/login"><img src={Applogo} className="pk-logo" alt="PARTICIPATION HOUSE SUPPORT SERVICES" /></a>
            </div>
            {/* /Account Logo */}
            <div className="account-box">
              <div className="account-wrapper">
                <h3 className="account-title">Reset Password</h3>
                <p className="account-subtitle"></p>
                {/* Account Form */}
                <form action="/app/main/dashboard">
                
                  <div className="form-group">
                    <label>New password</label>
                    <input className="form-control" type="password" />
                  </div>
                  <div className="form-group">
                    <label>Confirm Password</label>
                    <input className="form-control" type="password" />
                  </div>
                  <div className="form-group text-center">
                    <button className="btn btn-primary account-btn" type="submit">Reset Password</button>
                  </div>
                  <div className="account-footer">
                    <p>Already have an account? <a href="/login">Login</a></p>
                  </div>
                </form>
                {/* /Account Form */}
              </div>
            </div>
          </div>
        </div>
      </div>
      );
   }
}

export default Loginpage;
