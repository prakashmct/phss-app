
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {  Avatar_01 ,Avatar_02,Avatar_03,Avatar_04, Avatar_05, Avatar_08, Avatar_09, Avatar_10,
    Avatar_11,Avatar_12,Avatar_13,Avatar_16   } from "../../Entryfile/imagepath"

import { Table } from 'antd';
import 'antd/dist/antd.css';
import {itemRender,onShowSizeChange} from "./pagination/paginationfunction"
import "../MainPage/antdstyle.css"
import { Multiselect } from 'multiselect-react-dropdown';

import Select from 'react-select-me';
import 'react-select-me/lib/ReactSelectMe.css';

import Header from '../Sidebar/header.jsx';
import SidebarContent from '../Sidebar/sidebar';

import Loader from '../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../Helpers/SystemHelper';
import FileUploadHelper from '../Helpers/FileUploadHelper';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import moment from 'moment';

import SelectDropdown from 'react-dropdown-select';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

class AuditReports extends Component {
  constructor(props) {
    super(props);
    this.state = {

        // Pagination
        totalCount : 0,
        pageSize : 10,
        currentPage : 1,
        totalPages : 0,
        previousPage : false,
        nextPage : false,
        searchText : '',
        pagingData : {},
        TempsearchText:'',

        sortColumn : '',
        SortType : false,
        IsSortingEnabled : true,
        // Pagination


        locationID:localStorage.getItem("primaryLocationId"),
        staffContactID:localStorage.getItem("contactId"),
        fullName:localStorage.getItem("fullName"),

        role_auditreports_can : {},
        role_employees_can : {},
        role_hierarchy_can : {},

        errormsg :  '',
        isDelete : false,
        ListGrid : [],

        header_data : [],
        
        FilterStartDate : '',
        FilterEndDate : '',
        FilterEntityComponent : '',
        FiltersearchText:'',
        FilterSortType : true,

        ExportFilter : {}
    };

    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
    
    this.onChangeSearch = this.onChangeSearch.bind(this);
  }

  onChangeSearch(value) {
    console.log(value);
    this.setState({ value });
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
    //console.log('handleChange');
    //console.log(input);

    if (this.state[input] != '') {
      delete this.state.errormsg[input];
    }

    if([input]=="pageSize")
    {
      this.setState({ current_page: 1 });
      this.GetReportListGrid(1,e.target.value,this.state.searchText)
    }
  }

  componentDidMount() {
    console.log("Reports");

    /* Role Management */
    console.log('Role Store reports_can');
    var getrole = SystemHelpers.GetRole();
    let audit_report_menu_can = getrole.audit_report_menu_can;
    this.setState({ role_auditreports_can: audit_report_menu_can });
    console.log(audit_report_menu_can);

    console.log('Role Store hierarchy_can');
    var getrole_hierarchy = SystemHelpers.GetRole();
    let hierarchy_can = getrole_hierarchy.hierarchy_can;
    this.setState({ role_hierarchy_can: hierarchy_can });
    console.log(hierarchy_can);
    /* Role Management */
    
    
    // Table header
    // Table header
  }

  
  
  

  //GetReportListGrid = () => e => {
  //GetReportListGrid = (currentPage,pageSize,searchText)  => e => {
  GetReportListGrid (currentPage,pageSize,searchText){
    

      //e.preventDefault();

      this.setState({ ListGrid : [] });
      this.setState({ ExportFilter: {} });

      let step1Errors = {};
    
      if (this.state["FilterEntityComponent"] =='') {
        step1Errors["FilterEntityComponent"] = "Please select Component.";
      }
      if (this.state["FilterStartDate"] =='') {
        step1Errors["FilterStartDate"] = "Please select Start Date.";
      }
      if (this.state["FilterEndDate"] =='') {
        step1Errors["FilterEndDate"] = "Please select End Date.";
      }

      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      /* Role Management */

      // Pagination
      let bodyarray = {};
      bodyarray["currentPage"] = 1;
      bodyarray["nextPage"] = false;
      bodyarray["pageSize"] = 10;
      bodyarray["previousPage"] = false;
      bodyarray["totalCount"] = 0;
      bodyarray["totalPages"] = 0;
      
      this.setState({ pagingData : bodyarray });

      this.setState({ currentPage: currentPage });
      this.setState({ pageSize: pageSize });

      var sort_Column = this.state.sortColumn;
      var Sort_Type = this.state.SortType;
      
      var IsSortingEnabled = true;

      var url_paging_para = '&pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&sortColumn='+sort_Column+'&SortType='+Sort_Type+'&IsSortingEnabled='+IsSortingEnabled;
      // Pagination
      
      var FilterEntityLogicalName = this.state['FilterEntityComponent'];

      if(this.state["FilterStartDate"] != ""){
        var FilterStartDate = moment(this.state["FilterStartDate"]).format("YYYY-MM-DD");
      }else{
        var FilterStartDate = "";
      }
      
      if(this.state["FilterEndDate"] !=""){
        var FilterEndDate = moment(this.state["FilterEndDate"]).format("YYYY-MM-DD");
      }else{
        var FilterEndDate = "";
      }
      

      var paraEntityLogicalName = 'entityLogicalName='+FilterEntityLogicalName;
      var paraStartDate = '&startDate='+FilterStartDate;
      var paraEndDate = '&endDate='+FilterEndDate;
      
      //ExportFilter
      let ExportFilterArray = {
        FilterEntityLogicalName: FilterEntityLogicalName,
        FilterStartDate: FilterStartDate,
        FilterEndDate: FilterEndDate,
      };
      this.setState({ ExportFilter: ExportFilterArray });
      //ExportFilter

      var pass_url = 'GetFieldHistory?'+paraEntityLogicalName+paraStartDate+paraEndDate+url_paging_para;
      console.log(pass_url);
      //return false;

      this.showLoader();
      var url=process.env.API_API_URL+pass_url;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetFieldHistory");
          console.log(data);
          if (data.responseType === "1") {
              //if(data.data.exportTimesheetReportLocationView != null)
              //{
                if(data.data !=null){
                  this.setState({ ListGrid: data.data });
                }
                if(data.pagingData !=null){
                  this.setState({ pagingData: data.pagingData });
                }
                
              //}
              
              
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  ExportReportData = (fileType) => e => {

      e.preventDefault();
      
      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      let canDelete = getrole.employees_can.employees_can_delete;
      /* Role Management */

      var FilterPara = this.state.ExportFilter;
      console.log('payload GetFieldHistory');
      console.log(FilterPara);
      //console.log(FilterPara.FilterPayPeriod);
      //return false;
      
      var paraEmploymentType = '&entityLogicalName='+FilterPara.FilterEntityLogicalName;
      var paraStartDate = '&startDate='+FilterPara.FilterStartDate;
      var paraEndDate = '&endDate='+FilterPara.FilterEndDate;
      var parafileType = '&exportType='+fileType;
      
      var pass_url = 'GetAuditReportDataExport?'+paraEmploymentType+paraStartDate+paraEndDate+parafileType;
      console.log(pass_url);
      //return false;

      this.showLoader();
      var url=process.env.API_API_URL+pass_url;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetAuditReportDataExport");
          console.log(data);
          if (data.responseType === "1") {

              if(data.data != null){
                
                
                var File_Name= 'AuditReport';

                var createBase64 = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,"+data.data;
                var ext=File_Name+'.xlsx';
                
                
                const linkSource = createBase64;
                const downloadLink = document.createElement("a");
                const fileName = ext;

                downloadLink.href = linkSource;
                downloadLink.download = fileName;
                downloadLink.click();

              }
              
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        console.log(error);
        this.props.history.push("/error-500");
      });
  }

 

  // Pagination Design
  PaginationDesign ()
  {
    let PageOutput = [];
    //console.log('pagination');
    //console.log(this.state.pagingData);
    
    if(this.state.pagingData !="" && this.state.pagingData !="undefined")
    {
      var Page_Count = this.state.pagingData.totalPages;
      //alert(this.state.pagingData.currentPage);
    //console.log('page count = ' + Page_Count);
    /* pagination count */


        var Page_Start=1;
        var Page_End=1;

        if(this.state.pagingData.currentPage == 1){
            Page_Start=1;

            if(Page_Count <= 10){
                Page_End=Page_Count;
            }else{
                Page_End=10;
            }
            
        }else{

            if(this.state.pagingData.currentPage < 5){
                Page_Start=1;
                Page_End=Page_Count;
                //console.log("Page_End 1 "+ Page_End);
            }else{
                Page_Start=parseInt(this.state.pagingData.currentPage) - parseInt(4);
                Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
                //console.log("Page_End 2 "+ Page_End);
                if(Page_End > Page_Count){
                    Page_End=Page_Count;
                    //console.log("Page_End 3 "+ Page_End);
                }
            }

        }
      let Page = [];
      var i = 1;
      for (var z=Page_Start; z <= Page_End ; z++)
      {
        if(z==this.state.pagingData.currentPage)
        {
          Page.push(<li className="page-item active pk-active">
            <a className="page-link pk-active" id={z} href="#" onClick={this.PageGetGridData}>{z}<span className="sr-only">(current)</span></a>
          </li>);
        }
        else
        {
          Page.push(<li className="page-item"><a className="page-link" id={z} href="#" onClick={this.PageGetGridData} >{z}</a></li>);
        }
        i++;
      }

      let PagePrev = [];

      if(this.state.pagingData.currentPage == 1){
        PagePrev.push(<li className="page-item disabled">
          <a className="page-link" href="#">Previous</a>
        </li>);
      }else{
        PagePrev.push(<li className="page-item">
          <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)-parseInt(1)} tabIndex={-1} onClick={this.PageGetGridData}>Previous</a>
        </li>);
      }

      let PageNext = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageNext.push(<li className="page-item disabled">
          <a className="page-link" href="#">Next</a>
        </li>);
      }else{
        PageNext.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)+parseInt(1)} onClick={this.PageGetGridData}>Next</a>
          </li>
        );
      }

      let PageLast = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageLast.push(<li className="page-item disabled">
          <a className="page-link" href="#">Last</a>
        </li>);
      }else{
        PageLast.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(Page_Count)} onClick={this.PageGetGridData}>Last</a>
          </li>
        );
      }



      PageOutput.push(<section className="comp-section" id="comp_pagination">
                        <div className="pagination-box">
                          <div>
                            <ul className="pagination">
                              
                              {PagePrev}
                              {Page}
                              {PageNext}
                              {PageLast}
                              
                            </ul>
                          </div>
                        </div>
                      </section>);
    }
    
    return PageOutput;
  }

  PageGetGridData = e => {

    e.preventDefault();
    let current_page= e.target.id;
    this.GetReportListGrid(current_page,this.state.pageSize,this.state.searchText);
  }

  SearchGridData = () => e => {
    e.preventDefault();

    this.setState({ pageSize: this.state.TempsearchText });
    this.GetReportListGrid(this.state.currentPage,this.state.pageSize,this.state.searchText);
  }
  // Pagination Design

  render() {

    

    return ( 
      <div className="main-wrapper">
     
        {/* Toast & Loder method use */}
          
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}
        <Header/>

        <div className="page-wrapper">
            <Helmet>
                <title>{process.env.WEB_TITLE}</title>
                <meta name="description" content="Login page"/>         
            </Helmet>
              {/* Page Content */}
              <div className="content container-fluid">
                {/* Page Header */}
                <div className="page-header">
                  <div className="row align-items-center">
                    <div className="col">
                      <h3 className="page-title">Reports</h3>
                      <ul className="breadcrumb">
                        <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li className="breadcrumb-item active">AUDIT Reports</li>
                      </ul>
                    </div>
                    <div className="col-auto float-right ml-auto">
                      
                    </div>
                  </div>
                </div>

                {/* Search Filter */}
                  <div className="row filter-row">
                    
                    <div className="col-lg-2 col-sm-4 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <input className="form-control" type="date" id="FilterStartDate" value={this.state.FilterStartDate}  onChange={this.handleChange('FilterStartDate')} />
                        <label className="focus-label">Start date</label>
                      </div>
                      <span className="form-text error-font-color">{this.state.errormsg["FilterStartDate"]}</span>
                    </div>
                    <div className="col-lg-2 col-sm-4 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <input className="form-control" type="date" id="FilterEndDate" value={this.state.FilterEndDate}  onChange={this.handleChange('FilterEndDate')} min={moment().format(this.state.FilterStartDate,"YYYY-MM-DD")} />
                        <label className="focus-label">End date</label>
                      </div>
                      <span className="form-text error-font-color">{this.state.errormsg["FilterEndDate"]}</span>
                    </div>
                    <div className="col-lg-2 col-sm-4 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" id="FilterEntityComponent" value={this.state.FilterEntityComponent} onChange={this.handleChange('FilterEntityComponent')} > 
                          <option value="">-</option>
                          <option value="contact">Contact</option>
                          <option value="phss_address">Address</option>
                        </select>
                        <label className="focus-label">Select Component</label>
                      </div>
                      <span className="form-text error-font-color">{this.state.errormsg["FilterEntityComponent"]}</span>
                    </div>
                    <div className="col-lg-2 col-sm-4 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" id="SortTypeId" value={this.state.FilterSortType} onChange={this.handleChange('FilterSortType')}> 
                          <option value="">-</option>
                          <option value="false">Ascending</option>
                          <option value="true">Descending</option>
                        </select>
                        <label className="focus-label">Sorting Order</label>
                      </div>
                    </div>
                    <div className="col-lg-2 col-sm-4 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <input className="form-control floating" type="text" value={this.state.TempsearchText}  onChange={this.handleChange('TempsearchText')}/>
                        <label className="focus-label">Search</label>
                      </div>
                    </div>
                    
                    <div className="col-lg-2 col-sm-4 col-xs-12">  
                      <a href="#" className="btn btn-success btn-block" onClick={this.SearchGridData()}> Search </a>  
                    </div> 

                    

                  </div>
                {/* /Search Filter */}  

                <div className="col-md-2">
                    <div className="form-group">
                      <label>Per Page</label>
                      <select className="form-control floating" value={this.state.pageSize}  onChange={this.handleChange('pageSize')}> 
                        <option value="5">5/Page</option>
                        <option value="10">10/Page</option>
                        <option value="50">50/Page</option>
                        <option value="100">100/Page</option>
                      </select>
                    </div>
                  </div>

                {/* /Page Header */}

                {this.state.ListGrid.length > 0 ?
                  <div className="row">
                      <div className="col-sm-12">  
                        <a href="#" className="btn btn-danger mr-1 float-right" onClick={this.ExportReportData('EXCEL')}> Export EXCEL </a>
                        {/*<a href="#" className="btn btn-danger mr-1 float-right" onClick={this.ExportReportData('PDF')}> Export PDF </a>  */}
                      </div>
                  </div>
                  : null
                }
                <div className="row">
                  <div className="col-md-12">
                      <div className="table-responsive pk-overflow-hide">
                        {this.state.ListGrid.length > 0 ?
                          <table className="table table-striped custom-table mb-0 datatable">
                            <thead>
                              <tr>
                                <th>Staff Id</th>
                                <th>Full Name</th>
                                <th>Field Name</th>
                                <th>Change From</th>
                                <th>Change To</th>
                                <th>Update By</th>
                                <th>Update Date</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.ListGrid.map(( listValue, index ) => {
                                return (
                                  <tr key={index}>
                                    
                                    <td className="text-align">{listValue.staffId}</td>
                                    <td className="text-align">{listValue.fullName}</td>
                                    <td className="text-align">{listValue.fieldName}</td>
                                    <td className="text-align">{listValue.oldvalue}</td>
                                    <td className="text-align">{listValue.newValue}</td>
                                    <td className="text-align">{listValue.changedBy}</td>
                                    <td className="text-align">{moment(listValue.changedOn).format(process.env.DATE_TIME_FORMAT)}</td>
                                    <td className="text-align">{listValue.operation}</td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                          : null
                        }

                        {/* Pagination */}
                        {this.PaginationDesign()}
                        {/* /Pagination */}

                    </div>
                  </div>
                </div>
              </div>
              {/* /Page Content */}

              {/* Add Reports Modal */}
              {/* /Add Reports Modal */}

              {/* Edit Reports Modal */}
              {/* /Edit Reports Modal */}

              {/* Delete Today Work Modal */}
              {/* Delete Today Work Modal */}
            </div>
          <SidebarContent/>
      </div>
        );
      
   }
}

export default AuditReports;
