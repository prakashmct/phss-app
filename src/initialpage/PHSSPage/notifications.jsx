
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {  Avatar_01 ,Avatar_02,Avatar_03,Avatar_04, Avatar_05, Avatar_08, Avatar_09, Avatar_10,
    Avatar_11,Avatar_12,Avatar_13,Avatar_16,ProfileImageFemale,ProfileImage   } from "../../Entryfile/imagepath"

import { Table } from 'antd';
import 'antd/dist/antd.css';
import {itemRender,onShowSizeChange} from "./pagination/paginationfunction"
import "../MainPage/antdstyle.css"
import { Multiselect } from 'multiselect-react-dropdown';

import Select from 'react-select-me';
import 'react-select-me/lib/ReactSelectMe.css';

import Header from '../Sidebar/header.jsx';
import SidebarContent from '../Sidebar/sidebar';


import Loader from '../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../Helpers/SystemHelper';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import moment from 'moment';

class Notifications extends Component {
  constructor(props) {
    super(props);
    this.state = {

        // Pagination
        totalCount : 0,
        pageSize : 5,
        currentPage : 1,
        totalPages : 0,
        previousPage : false,
        nextPage : false,
        searchText : '',
        pagingData : {},
        TempsearchText:'',
        // Pagination

        errormsg :  '',
        userTypeList : [],
        roleList : [],

        ListGrid: []
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
       
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
    //console.log(input);
    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method
  
  
  componentDidMount()
  {
    
    this.GetUserWiseNotificationListForPage(1);
    
  }

  GetUserWiseNotificationListForAlert1(){

      this.showLoader();
      var url=process.env.API_API_URL+'GetUserWiseNotificationListForAlert'+'?contactId='+localStorage.getItem("contactId");
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetUserWiseNotificationListForAlert");
          console.log(data);
          if (data.responseType === "1") {
              //his.setState({ ListGrid: data.data });
              this.setState({ ListGrid: this.rowData(data.data) })
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        console.log("responseJson GetUserWiseNotificationListForAlert catch error");
        console.log(error);
        this.props.history.push("/error-500");
      });
  }

  GetUserWiseNotificationListForPage(currentPage){

      this.showLoader();   

      
      
      var PageNumber = currentPage;
      var PageSize = this.state.pageSize;
      var SearchText = "";
      var SortColumn = "";
      var SortType = true;
      var IsSortingEnabled = false;
      this.setState({ currentPage: currentPage });
      
      var par = '&pageNumber='+PageNumber+'&pageSize='+PageSize+'&searchText='+SearchText+'&sortColumn='+SortColumn+'&SortType='+SortType+'&IsSortingEnabled='+IsSortingEnabled;
      var url=process.env.API_API_URL+'GetUserWiseNotificationListForPage'+'?contactId='+localStorage.getItem("contactId")+par;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetUserWiseNotificationListForPage");
          console.log(data);
          if (data.responseType === "1") {
              //his.setState({ ListGrid: data.data });
              this.setState({ ListGrid: this.rowData(data.data) });

              this.setState({ pagingData: data.pagingData });

              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        console.log("responseJson GetUserWiseNotificationListForPage catch error");
        console.log(error);
        this.props.history.push("/error-500");
      });
  }

  

  rowData(ListGrid) {
      console.log(ListGrid)
      
      
      var ListGrid_length = ListGrid.length;
      let dataArray = [];
      var i=1;
      for (var z = 0; z < ListGrid_length; z++) {
        var tempdataArray = [];
        //tempdataArray.rownum = i;
        tempdataArray.createdOn = ListGrid[z].createdOn;
        tempdataArray.isRead = ListGrid[z].isRead;
        tempdataArray.notificationText = ListGrid[z].notificationText;
        tempdataArray.notificationType = ListGrid[z].notificationType;
        tempdataArray.notificationTitle = ListGrid[z].notificationTitle;
        tempdataArray.notificationPriority = ListGrid[z].notificationPriority;
        dataArray.push(tempdataArray);
        i++;
      }

      return dataArray;
  }


  // Pagination Design
  PaginationDesign ()
  {
    let PageOutput = [];
    console.log('pagination');
    console.log(this.state.pagingData);
    
    if(this.state.pagingData !="" && this.state.pagingData !="undefined" && this.state.pagingData.totalPages > 0)
    {
      var Page_Count = this.state.pagingData.totalPages;
      //alert(this.state.pagingData.currentPage);
    //console.log('page count = ' + Page_Count);
    /* pagination count */


        var Page_Start=1;
        var Page_End=1;

        if(this.state.pagingData.currentPage == 1){
            Page_Start=1;

            if(Page_Count <= 10){
                Page_End=Page_Count;
            }else{
                Page_End=10;
            }
            
        }else{

            if(this.state.pagingData.currentPage < 5){
                Page_Start=1;
                Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);

                if(Page_Count < Page_End){
                  Page_End = Page_Count
                }
                //Page_End=Page_Count;
                //console.log("Page_End 1 "+ Page_End);
            }else{
                Page_Start=parseInt(this.state.pagingData.currentPage) - parseInt(4);
                Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
                //console.log("Page_End 2 "+ Page_End);
                if(Page_End > Page_Count){
                    Page_End=Page_Count;
                    //console.log("Page_End 3 "+ Page_End);
                }
            }

        }
      let Page = [];
      var i = 1;
      for (var z=Page_Start; z <= Page_End ; z++)
      {
        if(z==this.state.pagingData.currentPage)
        {
          Page.push(<li className="page-item active pk-active"><a className="page-link pk-active" id={z} href="#" onClick={this.PageGetGridData} >{z}<span className="sr-only">(current)</span></a></li>);
        }
        else
        {
          Page.push(<li className="page-item"><a className="page-link" id={z} href="#" onClick={this.PageGetGridData} >{z}</a></li>);
        }
        i++;
      }

      let PagePrev = [];

      if(this.state.pagingData.currentPage == 1){
        PagePrev.push(<li className="page-item disabled">
          <a className="page-link" href="#">Previous</a>
        </li>);
      }else{
        PagePrev.push(<li className="page-item">
          <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)-parseInt(1)} tabIndex={-1} onClick={this.PageGetGridData}>Previous</a>
        </li>);
      }

      let PageNext = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageNext.push(<li className="page-item disabled">
          <a className="page-link" href="#">Next</a>
        </li>);
      }else{
        PageNext.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)+parseInt(1)} onClick={this.PageGetGridData}>Next</a>
          </li>
        );
      }

      let PageLast = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageLast.push(<li className="page-item disabled">
          <a className="page-link" href="#">Last</a>
        </li>);
      }else{
        PageLast.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(Page_Count)} onClick={this.PageGetGridData}>Last</a>
          </li>
        );
      }



      PageOutput.push(<section className="comp-section" id="comp_pagination">
                        <div className="pagination-box">
                          <div>
                            <ul className="pagination">
                              
                              {PagePrev}
                              {Page}
                              {PageNext}
                              {PageLast}
                              
                            </ul>
                          </div>
                        </div>
                      </section>);
    }
    
    return PageOutput;
  }



  PageGetGridData = e => {

    e.preventDefault();
    console.log(e.target.id);
    let current_page= e.target.id;
    this.GetUserWiseNotificationListForPage(current_page);
    
  }

  // Pagination Design

  render() {
      
                        
    return ( 
      <div className="main-wrapper">
        {/* Toast & Loder method use */}
          
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}
        <Header/>
        <div className="page-wrapper">
            <Helmet>
                <title>{process.env.WEB_TITLE}</title>
                <meta name="description" content="Login page"/>         
            </Helmet>
              {/* Page Content */}
              <div className="content container-fluid">
                {/* Page Header */}
                <div className="page-header">
                  <div className="row align-items-center">
                    <div className="col">
                      <h3 className="page-title">Notification </h3>
                      <ul className="breadcrumb">
                        <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li className="breadcrumb-item active">Notification </li>
                      </ul>
                    </div>
                  </div>
                </div>
                {/* /Page Header */}
                <div className="row">
                  <div className="col-md-12">
                    
                    <div className="activity">
                     <div className="activity-box">
                       <ul className="activity-list">
                          {this.state.ListGrid.map(( listValue, index ) => {
                            let noti_prioroty = "fa fa-flag";
                            if(listValue.notificationPriority == 'High'){
                               noti_prioroty = "fa fa-flag pk_notification_HighPriority";
                            }else if(listValue.notificationPriority == "Medium"){
                               noti_prioroty = "fa fa-flag pk_notification_MediumPriority";
                            }else if(listValue.notificationPriority == "Low"){
                               noti_prioroty = "fa fa-flag pk_notification_LowPriority";
                            }
                            return (<li>
                                  <div className="activity-user">
                                   <a href="#" title="Lesley Grauer" data-toggle="tooltip" className="avatar">
                                      {localStorage.getItem("userGender") == "Female" ?
                                        <img src={ProfileImageFemale} alt="" /> :
                                        <img src={ProfileImage} alt="" />
                                      }
                                   </a>
                                  </div>
                                  <div className="activity-content">
                                    
                                     <div className="timeline-content">
                                        <div className="row">
                                          <div className="col-md-11 notification-color">
                                            <a href="#" className="name notification-color"></a> {listValue.notificationTitle}
                                          </div>
                                          <div className="col-md-1">
                                            <i className={noti_prioroty}/>
                                            
                                          </div>
                                        </div>
                                        <span className="time notification-color">{listValue.notificationText}</span>
                                       <span className="time notification-color">{moment(listValue.createdOn).format("YYYY-MM-DD HH:mm A")}</span>
                                     </div>
                                     
                                    
                                  </div>
                                  </li>);
                            })
                          }
                          
                         
                      </ul>
                    </div>
                    {/* Pagination */}
                    {this.PaginationDesign()}
                    {/* /Pagination */}
                    </div>

                  </div>
                </div>
              </div>
              {/* /Page Content */}
              
            </div>
          <SidebarContent/>
      </div>
        );
      
   }
}

export default Notifications;
