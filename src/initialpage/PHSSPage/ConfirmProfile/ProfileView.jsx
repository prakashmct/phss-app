/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import { Multiselect } from 'multiselect-react-dropdown';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import moment from 'moment';
import InputMask from 'react-input-mask';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';



import Loader from '../../Loader';
import SystemHelpers from '../../Helpers/SystemHelper';

class ProfileView extends Component {
  constructor(props) {
    super(props);

    this.state = { Preferred_Language_Model: '' };

    this.state = {
        
        errormsg :  '',
        user_role: [],
        staffContactID:this.props.staffContactID,
        all_data :this.props.all_data,

        // Edit
        EditPreferredName :  '',
        EditDateOfBirth :  '',
        EditFirstName :  '',
        EditLastName :  '',
        // Edit

        //Display
        DisplayPreferredName:'',
        DisplayDateOfBirth:'',
        DisplayFirstName : '',
        DisplayLastName : '', 
        //Display

        role_profile_info_can : {}
        
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

   // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  // toast hide show method
  ToastSuccess(msg){
      toast.success(msg, {
        position: "top-right",
        autoClose: process.env.API_TOAST_TIME,
        hideProgressBar: false,
        closeOnClick: false,
        pauseOnHover: false,
        draggable: false
      });
  }

  ToastError(msg){
      toast.error(msg, {
        position: "top-right",
        autoClose: process.env.API_TOAST_TIME,
        hideProgressBar: false,
        closeOnClick: false,
        pauseOnHover: false,
        draggable: false
      });
  }
  // toast hide show method

  componentDidMount() {

    /* Role Management */
    console.log('Role Store profile_info_can');
    console.log(this.props.profile_info_can);
    this.setState({ role_profile_info_can: this.props.profile_info_can });
    /* Role Management */
    
    console.log("Confirm ProfileView");
    console.log(this.state.all_data);

    // Display
    this.setState({ DisplayPreferredName: this.state.all_data.preferredName });
    this.setState({ DisplayDateOfBirth: moment(this.state.all_data.dateOfBirth).format(process.env.DATE_FORMAT) });
    this.setState({ DisplayFirstName: this.state.all_data.firstName });
    this.setState({ DisplayLastName: this.state.all_data.lastName });
    //Display

    // Edit
    this.setState({ EditPreferredName: this.state.all_data.preferredName });

    if(this.state.all_data.dateOfBirth !='' && this.state.all_data.dateOfBirth != null){
      this.setState({ EditDateOfBirth: moment(this.state.all_data.dateOfBirth).format('YYYY-MM-DD') });
    }
    
    this.setState({ EditFirstName: this.state.all_data.firstName });
    this.setState({ EditLastName: this.state.all_data.lastName });
    // Edit

  }

  push_func(){
    this.props.history.push("/");
    this.hideLoader();
  }

  SessionOut(){
    this.showLoader();
  
    localStorage.removeItem("token");
    localStorage.removeItem("contactId");
    localStorage.removeItem("eMailAddress");
    localStorage.removeItem("fullName");

    //this.ToastError('Session Timeout');
    
    setTimeout( () => {this.push_func()}, 5000);
    
    return null;
  }

  //
  GetProfile(){
    console.log(localStorage.getItem("token"));
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserBasicInfoById?contactId='+this.state.staffContactID;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson Confirm GetUserBasicInfoById");
        console.log(data);

        if (data.responseType === "1")
        {
          this.setState({ all_data: data.data});
          
          localStorage.setItem("primaryLocationId", data.data.primaryLocationId);
          localStorage.setItem("isPayrollAdmin", data.data.isPayrollAdmin);
          
          this.setState({ DisplayPreferredName: data.data.preferredName });
          this.setState({ DisplayDateOfBirth: moment(this.state.all_data.dateOfBirth).format(process.env.DATE_FORMAT) });
          this.setState({ DisplayFirstName: data.data.firstName });
          this.setState({ DisplayLastName: data.data.lastName });
        }
        else
        {
          if(data.message == 'Authorization has been denied for this request.'){
            this.SessionOut();
          }else{
            this.ToastError(data.message);
          }
        }
        this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }
  //

  // Profile model Update api
  UpdateUserProfileTabProfileModel_API = () => e => {  
      e.preventDefault(); 
      
      let step1Errors = {};
      if (this.state["EditPreferredName"] === ''  || this.state["EditPreferredName"] == null) {
        step1Errors["EditPreferredName"] = "Preferred Name is mandatory"
      }
      if (this.state["EditDateOfBirth"] === '' || this.state["EditDateOfBirth"] == null) {
        step1Errors["EditDateOfBirth"] = "Date of Birth is mandatory"
      }
      if (this.state["EditFirstName"] === '' || this.state["EditFirstName"] == null) {
        step1Errors["EditFirstName"] = "First Name is mandatory"
      }
      if (this.state["EditLastName"] === '' || this.state["EditLastName"] == null) {
        step1Errors["EditLastName"] = "Last Name is mandatory"
      }
      
      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      this.showLoader();

      let User_Basic_profile = {
        preferredName: this.state["EditPreferredName"],
        dateOfBirth: this.state["EditDateOfBirth"],
        firstName: this.state["EditFirstName"],
        lastName: this.state["EditLastName"],
      };

      let bodyarray = {};
      bodyarray["contactId"] = this.state.staffContactID;
      bodyarray["userBasicInfo"] = User_Basic_profile;
      
      console.log(bodyarray);
      var url=process.env.API_API_URL+'UpdateUserBasicProfile';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
        console.log("responseJson Confirm UpdateUserBasicProfile");
        console.log(data);
        if (data.responseType === "1") {
          this.GetProfile();  
          this.ToastSuccess('Profile updated successfully');
          $( "#close_btn_profile" ).trigger( "click" );
        }
        else{
          this.ToastError(data.responseMessge);
        }
        this.hideLoader();
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }
  // Profile model Update api

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ errormsg: '' });

    // Display
    this.setState({ DisplayPreferredName: this.state.all_data.preferredName });
    this.setState({ DisplayDateOfBirth: moment(this.state.all_data.dateOfBirth).format(process.env.DATE_FORMAT) });
    this.setState({ DisplayFirstName: this.state.all_data.firstName });
    this.setState({ DisplayLastName: this.state.all_data.lastName });
    // Display 

    // Edit
    this.setState({ EditPreferredName: this.state.all_data.preferredName });
    this.setState({ EditDateOfBirth: moment(this.state.all_data.dateOfBirth).format('YYYY-MM-DD') });
    this.setState({ EditFirstName: this.state.all_data.firstName });
    this.setState({ EditLastName: this.state.all_data.lastName });
    // Edit
  }
  
   render() {
      return (
        
            <div className="col-md-6 d-flex">
              {(this.state.loading) ? <Loader /> : null}
              <div className="card profile-box flex-fill">
                <div className="card-body">
                {this.state.role_profile_info_can.profile_info_can_update == true ? 
                  
                  <h3 className="card-title">Profile <a href="#" className="edit-icon" data-toggle="modal" data-target="#ProfileTab_profile_modal"><i className="fa fa-pencil" /></a></h3>
                  : <h3 className="card-title">Profile <a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
                }
                  <ul className="personal-info">
                    <li>
                      <div className="title">Preferred Name</div>
                      {this.state.DisplayPreferredName == '' || this.state.DisplayPreferredName == null? <div className="text hide-font">None</div> : <div className="text">{this.state.DisplayPreferredName}</div> }
                    </li>
                    <li>
                      <div className="title">Name</div>
                      <div className="text">{this.state.DisplayFirstName} {this.state.DisplayLastName}</div>
                    </li>
                    <li>
                      <div className="title">Date of Birth</div>
                      {this.state.DisplayDateOfBirth == '' || this.state.DisplayDateOfBirth == null? <div className="text hide-font">None</div> : <div className="text">{this.state.DisplayDateOfBirth}</div> }
                    </li>
                    
                  </ul>
                </div>
              </div>

              {/* Staff Profile Info Modal */}
              <div id="ProfileTab_profile_modal" className="modal custom-modal fade" role="dialog">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title">Staff Profile Information</h5>
                      <button type="button" className="close" id="close_btn_profile" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()}>
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div className="modal-body">
                      <form>
                        <div className="card">
                          <div className="card-body">
                             <div className="row">
                              <div className="col-md-6">
                                <div className="form-group">
                                  <label>Preferred Name<span className="text-danger">*</span></label>
                                  <input type="text" className="form-control" value={this.state.EditPreferredName} onChange={this.handleChange('EditPreferredName')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["EditPreferredName"]}</span>
                                </div>
                              </div>
                              <div className="col-md-6">
                                <div className="form-group">
                                  <label>Date of Birth<span className="text-danger">*</span></label>
                                  <input className="form-control" type="date" value={this.state.EditDateOfBirth} onChange={this.handleChange('EditDateOfBirth')}/>
                                  <span className="form-text error-font-color">{this.state.errormsg["EditDateOfBirth"]}</span>
                                  
                                </div>
                              </div>
                              <div className="col-md-6">
                                <div className="form-group">
                                  <label>First Name<span className="text-danger">*</span></label>
                                  <input className="form-control" type="text"  value={this.state.EditFirstName} onChange={this.handleChange('EditFirstName')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["EditFirstName"]}</span>
                                </div>
                              </div>
                              <div className="col-md-6">
                                <div className="form-group">
                                  <label>Last Name<span className="text-danger">*</span></label>
                                  <input className="form-control" type="text" value={this.state.EditLastName} onChange={this.handleChange('EditLastName')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["EditLastName"]}</span>
                                </div>
                              </div>
                              
                              
                            </div>  
                            <div className="submit-section">
                              <button className="btn btn-primary submit-btn" type="submit" onClick={this.UpdateUserProfileTabProfileModel_API({})}>Submit</button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            {/* /Staff Profile Info Modal */}
            </div>
        
      );
   }
}

export default ProfileView;
