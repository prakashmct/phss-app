/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Consentsandwaivers from "./ConsentsAwards/ConsentsAndWaivers";
import AwardsAndRecognition from "./ConsentsAwards/AwardsAndRecognition";
import SystemHelpers from '../../Helpers/SystemHelper';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

class ConsentsAwards extends Component {
constructor(props) {
    super(props);

    this.state = {
        errormsg :  '',
        user_role: [],
        staffContactID:this.props.staffContactID,
        role_consents_waivers_can: {},
        role_awards_recognitions_can : {}
    };
    this.setPropState = this.setPropState.bind(this);
}

setPropState(key, value) {
      this.setState({ [key]: value });
}

componentDidMount() {
    // console.log("consents_waivers  awards_recognitions");
    // console.log(this.props.consents_waivers_can_create);
    // console.log(this.props.consents_waivers_can_update);
    // console.log(this.props.consents_waivers_can_view);
    // console.log(this.props.consents_waivers_can_delete);
    // console.log(this.props.consents_waivers_can_approve);
    // console.log(this.props.consents_waivers_can_export);

    // console.log(this.props.awards_recognitions_can_create);
    // console.log(this.props.awards_recognitions_can_update);
    // console.log(this.props.awards_recognitions_can_view);
    // console.log(this.props.awards_recognitions_can_delete);
    // console.log(this.props.awards_recognitions_can_approve);
    // console.log(this.props.awards_recognitions_can_export);
    /* Role Management */
    var pwd = localStorage.getItem("contactId")+"Phss@123";
    var Role_session = localStorage.getItem('sessiontoken');

    var _ciphertext = CryptoAES.decrypt(Role_session, pwd);
    console.log('Role Store consents_waivers_can role_awards_recognitions_can');
    //console.log(_ciphertext.toString(CryptoENC));
    var JsonCreate = JSON.parse(_ciphertext.toString(CryptoENC));
    this.setState({ role_consents_waivers_can: JsonCreate.consents_waivers_can });
    this.setState({ role_awards_recognitions_can: JsonCreate.awards_recognitions_can });
    
    console.log(JsonCreate.consents_waivers_can);
    console.log(JsonCreate.awards_recognitions_can);
    /* Role Management */

}
   render() {
     
      return (
        <div>
          <AwardsAndRecognition
            key={this.setPropState}
            staffContactID={this.state.staffContactID}

          	awards_recognitions_can={this.props.awards_recognitions_can}
            
            setPropState={this.setPropState}
           />
        </div>
      );
   }
}

export default ConsentsAwards;
